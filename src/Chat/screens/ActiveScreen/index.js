import React from 'react';
import './styles.css';
import UserInfoCard from "../../components/UserInfoCard";

const ActiveScreen = () => {
    const users = ['a', 'b', 'a', 'b', 'a', 'b', 'a', 'b', 'a', 'b', 'a', 'b',];
    return (
            <div className={'usersCardContainer'}>
                {
                    users.map((name) =>
                        <UserInfoCard/>
                    )
                }
            </div>
        );
};

export default ActiveScreen;
