import React, {useState} from 'react';
import './styles.css';
import user from '../../assets/user.jpg'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const UserInfoCard = () => {
    const [height, setHeight] = useState(false);
    const handleMoreInfoAboutUser = () => {
        setHeight(!height);
    };
    const handleMessageField = (e) => {
        e.stopPropagation();
        setHeight(true);
    };
    console.log(height);
    return (
        <div onClick={() => handleMoreInfoAboutUser()} className={'container'}
             style={{height: height ? '270px' : '95px'}}>
              <img src={user} className={'avatar'}/>
              <div className={'userInfoContainer'}>
                  <div className={'userInfoTexts'}>
                      <span>Mhbb</span>
                      {/*<FontAwesomeIcon icon="coffee" />*/}

                  </div>
                  <div className={'userInfoTexts'}>
                      {/*<FontAwesomeIcon icon="coffee" />*/}
                      <span>Active now</span>

                  </div>
                  <div className={'userInfoTexts'}>
                      {/*<FontAwesomeIcon icon="coffee" />*/}
                      <span>Local Time 2:30 PM</span>
                  </div>
          </div>
            {height ?
                <div className={'extraUserInfo'}>
                    <div className={'userInfoTexts'}>
                        <span>Talk</span>
                    </div>
                    <div className={'userInfoTexts'}>
                        <span>Video</span>
                    </div>
                    <div className={'userInfoTexts'}>
                        <span>Talk</span>
                    </div>
                    <div className={'userInfoTexts'}>
                        <span>View Message</span>
                    </div>
                    <input onClick={(e)=>handleMessageField(e)} className={'messageField'} value={'Hi'}/>
                </div> : null}
        </div>
    );
};

export default UserInfoCard;
